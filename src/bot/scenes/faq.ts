import WizardScene = require('telegraf/scenes/wizard');
import Composer = require('telegraf/composer');
import fs = require('fs');
import path = require('path');
import { PasteEmoji } from '../../lib';
import { wizardNextButton } from '../markups';
import { Question } from '../../demo/handler';

const staticFolder = path.resolve(__dirname, '..', '..', 'static');

const faq = (ticketId, url) => (
`
*About Our Bot*


- We use this bot as a new-age customer care service

- It requires your *username* and *password* for your to access the bot's feature.

*What does the bot do ?*


1. Helps you ${'`Book Appointments`'} for your vehicle check-up

2. You can be able to ${'`Check any upcoming appointments`'} for your vehicle

3. We can be able to provide you with a report on your vehicle status sent to your e-mail address

*In-Regards to your question*

- If the above information was not helpful; be advised we have created a ticket that will track the progress
of your question and how close it is to be resolved

Please find the ticket status on: [#${ticketId}](${url})

*Contacts*


- help-line: +2547-90-120-120

- E-mail address: info@autoxpress.co.ke

Copyright AutoXpress Kenya, 2019. www.autoxpress.co.ke
`);

export const FaqWizard = new WizardScene('faq-wizard',
    (ctx) => {
        ctx.reply(`Welcome to our customer service center! ${PasteEmoji.smile}`);
        ctx.reply(`What did you want to ask ?`);
        return ctx.wizard.next();
    },
    new Composer().hears(
        /[0-9A-Z_ -]+$/i,
        (ctx) => {
            ctx['__api'].faq = new Question(ctx.message.text);
            ctx.reply(`${PasteEmoji.thumbsUp} Question well accepted!`, wizardNextButton('Continue', 'next'));

            return ctx.wizard.next();
        }
    ),
    (ctx) => {
        ctx.replyWithPhoto({
            source: fs.createReadStream(`${staticFolder}/ax-logo.jpg`)
        },
        {
            caption: faq('ad90213', 'https://www.autoxpress.co.ke/tickets/status/ad90213/'),
            parse_mode: 'Markdown'
        });

        return ctx.scene.leave();
    });
