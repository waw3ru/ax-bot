import WizardScene = require('telegraf/scenes/wizard');
import Composer = require('telegraf/composer');
import { User } from '../../demo/handler';
import { PasteEmoji } from '../../lib';
import { mainMenu, loginButton } from '../markups';

const composers = {
    general: new Composer(),
    username: new Composer(),
    password: new Composer(),
};

composers.general.action('next', (ctx) => {
    return ctx.wizard.next();
});

composers.general.command('next', (ctx) => {
    return ctx.wizard.next();
});

composers.username.hears(/usr [0-9a-z_ -]+$/, (ctx) => {
    const username = ctx.match[0].split(' ')[1];
    ctx.__api.user = new User(username, '');
    ctx.replyWithMarkdown(`Thanks for that, Now enter your password. Format is */pwd [YOUR PASSWORD]*`);

    return ctx.wizard.next();
});

composers.password.hears(/pwd [0-9a-z_ -]+$/, (ctx) => {
    const password = ctx.match[0].split(' ')[1];
    ctx.__api.user.password = password;

    if (ctx.__api.user.loginUser()) {
        ctx.reply(`${PasteEmoji.finesse} Login was successful. Welcome, ${ctx.__api.user.username}`, mainMenu);
        return ctx.scene.leave();
    }

    ctx.reply(`Sorry but your username or password is wrong please try again`, loginButton);
    return ctx.scene.leave();
});


export const LoginWizard = new WizardScene('login-wizard',
    (ctx) => {
        ctx.replyWithMarkdown(`Welcome to the ${'`Login Wizard`'}. Enter your username in the form: */usr [YOUR USERNAME]*`);
        return ctx.wizard.next();
    },
    composers.username,
    composers.password);
