import { PasteEmoji } from '../lib';

import Markup = require('telegraf/markup');

export const mainMenu = Markup.keyboard(
    [
        [ `${PasteEmoji.bookmark} Book Appointment`, `${PasteEmoji.calendar} Upcoming Appointments`],
        [`${PasteEmoji.openFolder} Vehicle Report`, `${PasteEmoji.question} Ask a Question`],
        [`${PasteEmoji.disappointed} Logout`],
    ])
    .resize()
    .extra();

export const loginButton = Markup.keyboard(
    [ [`${PasteEmoji.penLock} Login`, `${PasteEmoji.bulb} About auto-express`] ])
    .resize()
    .extra();

export const wizardNextButton = (text, action) => Markup.inlineKeyboard(
    [ Markup.callbackButton(`${text}`, `${action}`) ]).extra();
