import { BOT } from '../bot';
import { PasteEmoji, aboutAutoXpress } from '../lib';
import { mainMenu, loginButton } from './markups';
import fs = require('fs');
import path = require('path');

const staticFolder = path.resolve(__dirname, '..', 'static');

BOT.command(['start', 'hello'], (ctx) => {
    if (!ctx['__api'].user) {
        return ctx.reply(`${PasteEmoji.slightSmile} Please login first to continue`, loginButton);
    }
    return ctx.reply(`${PasteEmoji.smile} Welcome back to Auto-Express Bot!, ${ctx['__api'].user.username}`, mainMenu);
});

(BOT as any).hears(`${PasteEmoji.penLock} Login`, (ctx) => ctx.scene.enter('login-wizard'));

(BOT as any).hears(`${PasteEmoji.question} Ask a Question`, (ctx) => ctx.scene.enter('faq-wizard'));

BOT.hears(`${PasteEmoji.bulb} About auto-express`, (ctx) =>
    ctx.replyWithPhoto({
        source: fs.createReadStream(`${staticFolder}/ax-logo.jpg`)
    },
    {
        caption: aboutAutoXpress,
        parse_mode: 'Markdown'
    } as any)
);

BOT.hears(`${PasteEmoji.openFolder} Vehicle Report`, (ctx) => {
    setTimeout(() =>
        ctx.replyWithMarkdown(`A report about your vehicle *history* and *status* has been sent to your registered e-mail address`),
    250);
});

BOT.hears(`${PasteEmoji.disappointed} Logout`, (ctx) => {
    ctx['__api'].user = undefined;
    ctx.reply(`${PasteEmoji.disappointed} Sorry to watch you leave! Come back later`);
    return ctx.reply(`Bye! ${PasteEmoji.thumbsUp}`, loginButton);
});
