import Telegraf from 'telegraf';
import { config } from './config';
import { demo } from './demo/handler';

import Stage = require('telegraf/stage');
import session = require('telegraf/session');
import { LoginWizard } from './bot/scenes/login';
import { FaqWizard } from './bot/scenes/faq';

const Bot = new Telegraf(config.botToken);
const stage = new Stage();

Bot.use(session());
Bot.context['__api'] = demo;
Bot.use(stage.middleware());

// stages
stage.register(LoginWizard);
stage.register(FaqWizard);

export {
    Bot as BOT,
    stage as STAGE,
};
