import dotenv = require('dotenv');

dotenv.config();

abstract class BaseConfig {

    public botToken = process.env.BOT_TOKEN;
    public botName = process.env.BOT_NAME || 'Auto-Express Bot';
}

class DevConfig extends BaseConfig {}

class ProdConfig extends BaseConfig {}

const Config = {

    development: new DevConfig(),
    production: new ProdConfig()

}[process.env.NODE_ENV || 'development'];

// Ensure that if NODE_ENV is not supported to assume Dev Configs
export const config = Config || new DevConfig();