import emoji = require('node-emoji');

export const PasteEmoji = {
    calendar: emoji.emojify(':calendar:'),
    stats: emoji.emojify(':chart_with_upwards_trend:'),
    openFolder: emoji.emojify(':open_file_folder:'),
    question: emoji.emojify(':question:'),
    bookmark: emoji.emojify(':bookmark:'),
    smile: emoji.emojify(':smile:'),
    slightSmile: emoji.emojify(':point_right:'),
    finesse: emoji.emojify(':ok_hand:'),
    thumbsUp: emoji.emojify(':+1:'),
    thumbsDown: emoji.emojify(':-1:'),
    congratulation: emoji.emojify(':clap:'),
    disappointed: emoji.emojify(':disappointed:'),
    heart: emoji.emojify(':heart:'),
    penLock: emoji.emojify(':lock_with_ink_pen:'),
    bulb: emoji.emojify(':bulb:'),
    feet: emoji.emojify(':feet:'),
    car: emoji.emojify(':red_car:'),
    wrench: emoji.emojify(':wrench:'),
};

export const aboutAutoXpress =
`
*Vision*

_To become the market leader in the East African region in the_:

1. Sales of Auto Parts and Accessories

2. Servicing of Vehicles


*Mission*

1. To deliver to our customers fast, efficient and truly superior service with an identifiable difference

2. To provide the appropriate product range for our customers

3. To employ, train and retain the very best human resource available

4. To utilize accurate processes and systems

`;