import { users } from './sample-data';

class DemoApi {
    user: User;
    faq: Question;
}

export class User {

    constructor(public username: string, public password: string) {}

    loginUser() {
       return users.filter(user => user.username === this.username && user.password === this.password).length === 1;
    }
}

export class Question {

    constructor(public txt: string) {}
}

export const demo = new DemoApi();
