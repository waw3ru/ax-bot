// const Telegraf = require('telegraf');
const path = require('path');
const __DEV__ = process.env.NODE_ENV === 'development';

let src;

if (__DEV__) {
    require('ts-node/register');
    src = path.resolve(__dirname, 'src');
} else {
    src = path.resolve(__dirname, 'out');
}

require(`${src}/bot/index`);
require(`${src}/bot`).BOT.launch();
